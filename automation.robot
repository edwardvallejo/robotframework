*** Settings ***
Library     String
Library     SeleniumLibrary

*** Variables ***
${browser}      Chrome
${home}         automationpractice.com/index.php
${scheme}       http
${testUrl}      ${scheme}://${home}

*** Keywords ***
Open home
    Open browser    ${testUrl}      ${browser}

*** Test Cases ***
C001 Hacer Click en Contenedores
    Open home
    Set Global Variable        @{nombreDeContenedores}     //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a        //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
    :FOR    ${nombredeContenedor}   IN      @{nombreDeContenedores}
    \   Click Element       xpath=${nombredeContenedor}
    \   Wait Until Element Is Visible   xpath=//*[@id="bigpic"]
    \   Click Element                   xpath=//*[@id="header_logo"]/a/img


